/** @file aspa_many.c
 *  @brief Function definitions for multi neuron spike trains
 *
 *  @author Christophe Pouzat <christophe.pouzat@parisdescartes.fr>
*/

#include "aspa.h"

/** @brief Allocates an [aspa_msta](@ref aspa_msta)
 *
 *  @returns a pointer to an allocated [aspa_msta](@ref aspa_msta).
*/
aspa_msta * aspa_msta_alloc(size_t n_neurons /**< [in] The number of neurons.*/)
{
  aspa_msta * msta = malloc(sizeof(aspa_msta));
  msta->n_neurons = n_neurons;
  msta->sta = malloc(n_neurons*sizeof(aspa_sta));
  return msta;
}


/** @brief Frees an [aspa_msta](@ref aspa_msta)
 *
 *  @returns 0 if everything goes fine 
*/
int aspa_msta_free(aspa_msta * msta /**< [in,out] A pointer to an allocated [aspa_msta](@ref aspa_msta)*/)
{
  for (size_t i=0; i < msta->n_neurons; i++)
    aspa_sta_free(msta->sta[i]);
  free(msta->sta);
  free(msta);
  return 0;
}

/** @brief Returns a pointer to a given [aspa_sta](@ref aspa_sta)  
 *         of an [aspa_msta](@ref aspa_msta).
 *
 *  Range checking is performed, if argument `sta_index` is too 
 *  large an error is generated.
 *  
 *  @returns a pointer to an [aspa_sta](@ref aspa_sta).  
*/
aspa_sta * aspa_msta_get_sta(const aspa_msta * msta, /**< [in] msta A pointer to an allocated [aspa_msta](@ref aspa_msta).*/
			     size_t sta_index /**< [in] sta_index the index of the requested [aspa_sta](@ref aspa_sta).*/)
{
  assert (sta_index < msta->n_neurons);
  return msta->sta[sta_index];
}


/** @brief Checks if members [aspa_sta](@ref aspa_sta) making 
 *         an [aspa_msta](@ref aspa_msta) are consistent.
 *
 *  To be consistent all the neurons (whose data are contained in
 *  the individual aspa_sta) should have the same values for:
 *   - `n_trials`
 *   - `onset`
 *   - `offset`
 *   - `trial_duration`
 * 
 *  as well as for each element of `trial_start_time`.
 *  Time comparisons are done with a precision given by `ASPA_EPSILON_TIME`.
 *  
 *  @returns 0 if the check is passed, 1 otherwise.  
*/
int aspa_msta_check(const aspa_msta * msta /**< [in] A pointer to an allocated [aspa_msta](@ref aspa_msta).*/)
{
  size_t n_neurons = msta->n_neurons;
  if (n_neurons==1) {
    /*If there is only one neuron there is nothing to check*/
    return 0;
  } else {
    size_t n_trials_ref = msta->sta[0]->n_trials;
    double onset_ref = msta->sta[0]->onset;
    double offset_ref = msta->sta[0]->offset;
    double trial_duration_ref = msta->sta[0]->trial_duration;
    double * trial_start_time_ref = msta->sta[0]->trial_start_time;
    for (size_t i=1; i < n_neurons; i++) {
      if (msta->sta[i]->n_trials != n_trials_ref) {
	fprintf(stderr,"Number of trials mismatch for neuron idx %ld.\n",i);
	return 1;
      }
      if (gsl_fcmp(msta->sta[i]->onset, onset_ref, ASPA_EPSILON_TIME) != 0) {
	fprintf(stderr,"Onset mismatch for neuron idx %ld.\n",i);
	return 1;
      }
      if (gsl_fcmp(msta->sta[i]->offset, offset_ref, ASPA_EPSILON_TIME) != 0) {
	fprintf(stderr,"Offset mismatch for neuron idx %ld.\n",i);
	return 1;
      }
      if (gsl_fcmp(msta->sta[i]->trial_duration, trial_duration_ref, ASPA_EPSILON_TIME) != 0) {
	fprintf(stderr,"Trial duration mismatch for neuron idx %ld.\n",i);
	return 1;
      }
      for (size_t j=0; j < n_trials_ref; j++) {
	if (gsl_fcmp(msta->sta[i]->trial_start_time[j],
		     trial_start_time_ref[j], ASPA_EPSILON_TIME) != 0) {
	  fprintf(stderr,"Trial start time mismatch for neuron idx %ld at trial %ld.\n",i,j);
	  return 1;
	}
      }
    }
  }
  return 0;
}

/** @brief Prints to stream the content of an [aspa_msta](@ref aspa_msta)
 *
 *  The first printed line is:
 *    - \# Number of neurons:
 *    .
 *  is followed by a blank line, then:
 *    - \# Start of neuron:
 *    - \# Number of trials:
 *    - \# Number of aggregated trials:
 *    - \# Stimulus onset:
 *    - \# Stimulus offset:
 *    - \# Single trial duration:
 *    .
 *  is printed first. The spike times (within trial times) of each
 *  trial are printed next with two blank lines separating the
 *  different trials (suitable for gnuplot). Each trial starts with 
 *  the following three comments lines:
 *    - \# Start of trial:
 *    - \# Trial start time:
 *    - \# Number of spikes:
 *    .
 *  and ends with:
 *    - \# End of trial:
 *    .
 *  At the end of the neuron's data we find:
 *    - \# End of neuron:
 *    .
 *  followed by a blank line before another neuron "starts".
 *
 *  @returns 0 if successful.  
*/
int aspa_msta_fprintf(FILE * stream, /**< [in,out] A pointer to an opened text file.*/
		      const aspa_msta * msta /**< [in] A pointer to the [aspa_msta](@ref aspa_msta) to be written.*/)
{
  size_t n_neurons = msta->n_neurons;
  fprintf(stream,"# Number of neurons: %ld\n\n", n_neurons);
  for (size_t i=0; i < n_neurons; i++) {
    fprintf(stream,"# Start of neuron: %ld\n", i);
    aspa_sta * sta = aspa_msta_get_sta(msta, i);
    aspa_sta_fprintf(stream, sta, false);
    fprintf(stream,"# End of neuron: %ld\n\n", i);
  }
  return 0;
}

/** @brief Scans multiple trials of many neurons from a text file and return the
 *         result in an allocated pointer to an [aspa_msta](@ref aspa_msta).
 *
 *  The expected format of the input is assumed to follow the following
 *  layout. A header whose first line start is:
 *    - \# Number of neurons:
 *    .
 *  is followed by a blank line, then:
 *    - \# Start of neuron:
 *    - \# Number of trials:
 *    - \# Number of aggregated trials:
 *    - \# Stimulus onset:
 *    - \# Stimulus offset:
 *    - \# Single trial duration:
 *    .
 *  is printed first. The spike times (within trial times) of each
 *  trial are printed next with two blank lines separating the
 *  different trials (suitable for gnuplot). Each trial starts with 
 *  the following three comments lines:
 *    - \# Start of trial:
 *    - \# Trial start time:
 *    - \# Number of spikes:
 *    .
 *  and ends with:
 *    - \# End of trial:
 *    .
 *  At the end of the neuron's data we find:
 *    - \# End of neuron:
 *    .
 *  followed by a blank line before another neuron "starts".
 * 
 *  @returns a pointer to an allocated [aspa_msta](@ref aspa_msta)
*/
aspa_msta * aspa_msta_fscanf(FILE * STREAM /**< [in/out] A pointer to an opened text file.*/)
{
  char buffer[256];
  char value[128];
  // Read line per line
  fgets(buffer, sizeof(buffer), STREAM);
  sscanf(buffer, "# Number of neurons:  %127s", value);
  size_t n_neurons = (size_t) atoi(value);
  aspa_msta * msta = aspa_msta_alloc(n_neurons);
  // Read one blank line
  fgets(buffer, sizeof(buffer), STREAM);
  for (size_t n_idx=0; n_idx < n_neurons; n_idx++) {
    // Read line "# Start of neuron: n_idx"
    fgets(buffer, sizeof(buffer), STREAM);
    // Read the aspa_sta of neuron n_idx
    msta->sta[n_idx] = aspa_sta_fscanf(STREAM);
    // Read two blank lines
    fgets(buffer, sizeof(buffer), STREAM);
    fgets(buffer, sizeof(buffer), STREAM);
    // Read line "# End of neuron: n_idx"
    fgets(buffer, sizeof(buffer), STREAM);
    // Read blank line
    fgets(buffer, sizeof(buffer), STREAM);
  }
  return msta;
}


/** @brief Prints to stream in binary format the content of an [aspa_msta](@ref aspa_msta)
 *
 *  A `size_t` with the number of neurons is printed first followed by the content of each
 *  individual [aspa_sta](@ref aspa_sta), one after the other.
 *
 *  @returns 0 if successful.  
*/
int aspa_msta_fwrite(FILE * stream, /**< [in,out] A pointer to an opened text file.*/
		     const aspa_msta * msta /**< [in] Pointer to the [aspa_msta](@ref aspa_msta) to be written.*/)
{
  size_t n_neurons = msta->n_neurons;
  fwrite(&n_neurons,sizeof(size_t),1,stream);
  for (size_t n_idx=0; n_idx < n_neurons; n_idx++)
    aspa_sta_fwrite(stream,msta->sta[n_idx],false);
  return 0;
}

/** @brief Reads multiple trials of many neurons from a binary file and return the
 *         result in an allocated pointer to an [aspa_msta](@ref aspa_msta)
 *
 *  The expected format of the input is assumed to follow the following
 *  layout: a `size_t` with the number of neurons followed by the [aspa_sta](@ref aspa_sta)
 *  of each individual neuron.
 *
 *  @returns A pointer to an allocated [aspa_msta](@ref aspa_msta).
*/
aspa_msta * aspa_msta_fread(FILE * STREAM /**< [in,out] A pointer to an opened text file*/)
{
  size_t n_neurons;
  fread(&n_neurons, sizeof(size_t),1,STREAM);
  aspa_msta * msta = aspa_msta_alloc(n_neurons);
  for (size_t n_idx=0; n_idx < n_neurons; n_idx++)
    msta->sta[n_idx] = aspa_sta_fread(STREAM);
  return msta;
}
