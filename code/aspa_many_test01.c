/** \file aspa_many_test01.c
 *  \brief Test functions [aspa_msta_fprintf](@ref aspa_msta_fprintf)
 *         and [aspa_msta_fscanf](@ref aspa_msta_fscanf)
 *
 *  The data should be downloaded first by typing in your bash shell
 *  something like:
 *
 *      prefix=https://raw.githubusercontent.com/christophe-pouzat/zenodo-locust-datasets-analysis
 *      prefix="$prefix"/master/Locust_Analysis_with_R/locust20010214/locust20010214_spike_trains/
 *      prefix="$prefix"locust20010214_Spontaneous_1_tetB_u
 *      for i in {2..7} ; do
 *          wget "$prefix"$i".txt"
 *      done
 *
 *  The program reads the data from the 7 specified neurons, allocates and initializes
 *  a [aspa_msta](@ref aspa_msta) writes it to a text file named `aspa_many_test01.txt`
 *  reads from the same file in a newly allocated ans initialized [aspa_msta](@ref aspa_msta)
 *  before computing and printing to the `stdout` the ISI statistics of each of the 
 *  7 neurons.
 *  This program is simply called from the command line with:
 *
 *      aspa_many_test01
 *
 *  @author Christophe Pouzat <christophe.pouzat@parisdescartes.fr>
 */

#include "aspa.h"

const char prefix[] = "locust20010214_Spontaneous_1_tetB_u";
const int neurons[] = {1,2,3,4,5,6,7};
const size_t nb_neurons = 7;
const char suffix[] = ".txt";
const double sample2second=15000.0;
const double trial_duration=29.0;
const double inter_trial_interval=30.0;
const double stim_onset=0.0, stim_offset=0.0;

int main()
{
  aspa_msta * msta = aspa_msta_alloc(nb_neurons);
  for (size_t n_idx=0; n_idx < nb_neurons; n_idx++) {
    char data_file[2048];
    strcpy(data_file,prefix);
    char number[1024];
    sprintf(number,"%d",neurons[n_idx]);
    strcat(data_file,number);
    strcat(data_file,suffix);
    FILE * fp = fopen(data_file,"r");
    printf("File %s opened.\n",data_file);
    gsl_vector * st_flat = aspa_raw_fscanf(fp,sample2second);
    fclose(fp);
    printf("File %s closed.\n",data_file);
    msta->sta[n_idx] = aspa_sta_from_raw(st_flat, inter_trial_interval,
					 stim_onset, stim_offset,
					 trial_duration);
    gsl_vector_free(st_flat);
  }
  printf("aspa_msta allocated and initialized.\n");
  int status = aspa_msta_check(msta);
  if (status == 0)
    printf("The data from the different neurons are consistent.\n");
  else
    printf("The data from the different neurons are not consistent.\n");
  char target[] = "aspa_many_test01.txt";
  FILE * fp = fopen(target,"w");
  printf("File %s opened for writing.\n",target);
  aspa_msta_fprintf(fp,msta);
  printf("aspa_msta written in %s.\n",target);
  fclose(fp);
  printf("File %s closed.\n",target);
  aspa_msta_free(msta);
  printf("Memory taken by aspa_msta freed.\n");
  fp = fopen(target,"r");
  printf("File %s opened for reading.\n",target);
  msta = aspa_msta_fscanf(fp);
  printf("aspa_msta reallocated and initialized with content of %s.\n",target);
  fclose(fp);
  printf("File %s closed.\n",target);
  status = aspa_msta_check(msta);
  if (status == 0)
    printf("The data from the different neurons after writing and reading are consistent.\n");
  else
    printf("The data from the different neurons after writing and reading are not consistent.\n");
  printf("Getting now the 5 numbers summaries for each neuron:\n\n");
  aspa_sta * sta;
  for (size_t n_idx=0; n_idx < nb_neurons; n_idx++) {
    char data_file[2048];
    strcpy(data_file,prefix);
    char number[1024];
    sprintf(number,"%d",neurons[n_idx]);
    strcat(data_file,number);
    strcat(data_file,suffix);
    printf("For the neuron whose data are stored in file %s we get:\n",data_file);
    sta = aspa_msta_get_sta(msta,n_idx);
    gsl_vector * isi = aspa_sta_isi(sta);
    aspa_fns isi_fns = aspa_fns_get(isi);
    if (sta->n_aggregated == 1)
      fprintf(stdout,"Data from %d trials.\n", (int) sta->n_trials);
    else
      fprintf(stdout,"Data from %d aggregated trials.\n", (int) sta->n_aggregated);
    fprintf(stdout,"The mean rate is: %4g Hz.\n", aspa_sta_rate(sta));
    fprintf(stdout,"The inter spike interval statistics are:\n"),
      aspa_fns_fprintf(stdout,&isi_fns);
    double src = aspa_lagged_spearman(isi, 1);
    fprintf(stdout,"A 95%% confidence interval for the lag 1 Spearman rank correlation is: [%g,%g].\n\n",
	    src-1.96*0.6325/sqrt(isi->size-1),src+1.96*0.6325/sqrt(isi->size-1));
    gsl_vector_free(isi); 
  }
  aspa_msta_free(msta);
  printf("Memory taken by aspa_msta freed.\n");
  exit (EXIT_SUCCESS);
}
