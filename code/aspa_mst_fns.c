/** @file aspa_mst_fns.c
 *  @brief User program for printing five number summaries together
 *         with a few extra-statistics for spike trains
 *
 *  @author Christophe Pouzat <christophe.pouzat@parisdescartes.fr>
*/
#include "aspa.h"

#include <getopt.h>

static char usage[] = \
  "usage: %s [-i --in_bin] [-s --single] [-n --neuron=integer]\n\n"
  "  -i --in_bin: specify binary data input format.\n"
  "  -m --many: specify input from an 'aspa_msta' instead of an\n"
  "     'aspa_sta' (the default).\n"
  "  -n --neuron <positive integer>: index of the considered neuron\n"
  "     if 'many' is set (default 0).\n\n"
  " The program reads data from the 'stdin'.\n"
  " The data can be either in 'txt' (default) or 'binary' format. In\n"
  " the latter case 'many' should be set.\n"
  " Data can correspond to several trials from a single neuron (an\n"
  " 'aspa_sta' structure) or several trials from many neurons (an\n"
  " 'aspa_msta' structure). Single neuron data are the default,\n"
  " set 'many' otherwise.\n"
  " When the data come from many neuron the neuron of interest can\n"
  " be specified with argument 'neuron' (default 0).\n"
  " The program reads the data and writes basic statistics to the\n"
  " 'stdout':\n"
  "   - the number of spikes,\n"
  "   - the mean freaquency,\n"
  "   - the inter spike interval (ISI) statistics,\n"
  "   - the spearman correlation coefficient between successive ISI.\n"
  "\n";
  
/** @brief Reads command line arguments.
 *  
 *  @return 0 when everything goes fine
*/
int read_args(int argc, /**< [in] Argument of main.*/
	      char ** argv, /**<  [in] Argument of main.*/
	      size_t * in_bin, /**< [out] Input format, 0 for "txt" 1 for "bin" (default 0).*/
	      size_t * many, /**< [out] Input type, 0 for 'aspa_sta', 1 of 'aspa_msta' (default 0).*/
	      size_t * neuron /**< [out] The neuron of interest (used only if 'many' is set to 1, default 0).*/)
{
  // Define default values
  *in_bin=0;
  *many=0;
  *neuron=0;
  {int opt;
    static struct option long_options[] = {
      {"in_bin",no_argument,NULL,'i'},
      {"many",no_argument,NULL,'m'},
      {"neuron",optional_argument,NULL,'n'},
      {"help",no_argument,NULL,'h'},
      {NULL,0,NULL,0}
    };
    int long_index =0;
    while ((opt = getopt_long(argc,argv,"himn:",long_options,\
			      &long_index)) != -1) {
      switch(opt) {
      case 'i': *in_bin=1;
	break;
      case 'm': *many=1;
	break;
      case 'n':
      {
	*neuron = (size_t) atoi(optarg);
      }
      break;
      case 'h': printf(usage,argv[0]);
	return -1;
      default : fprintf(stderr,argv[0]);
	return -1;
      }
    }
  }
  return 0;
}

int main(int argc, char ** argv)
{
  size_t in_bin,many,neuron;
  int status = read_args(argc,argv,&in_bin,&many,&neuron);
  if (status == -1) exit (EXIT_FAILURE);
  aspa_sta * sta;
  aspa_msta * msta;
  if (many == 0) { /* Data from a single neuron */
    if (in_bin == 0)
      sta = aspa_sta_fscanf(stdin);
    else
      sta = aspa_sta_fread(stdin);
  } else { /* Data from many neurons */
    if (in_bin == 0)
      msta = aspa_msta_fscanf(stdin);
    else
      msta = aspa_msta_fread(stdin);
    sta = aspa_msta_get_sta(msta,neuron);
  }
  gsl_vector * isi = aspa_sta_isi(sta);
  aspa_fns isi_fns = aspa_fns_get(isi);
  if (sta->n_aggregated == 1)
    fprintf(stdout,"Data from %d trials.\n", (int) sta->n_trials);
  else
    fprintf(stdout,"Data from %d aggregated trials.\n", (int) sta->n_aggregated);
  fprintf(stdout,"The mean rate is: %4g Hz.\n", aspa_sta_rate(sta));
  fprintf(stdout,"The inter spike interval statistics are:\n"),
  aspa_fns_fprintf(stdout,&isi_fns);
  double src = aspa_lagged_spearman(isi, 1);
  fprintf(stdout,"A 95%% confidence interval for the lag 1 Spearman rank correlation is: [%g,%g].\n",
	 src-1.96*0.6325/sqrt(isi->size-1),src+1.96*0.6325/sqrt(isi->size-1));
  gsl_vector_free(isi);
  if (many == 0) {
    aspa_sta_free(sta);
  } else {
    aspa_msta_free(msta);
  }
  return 0;
}

