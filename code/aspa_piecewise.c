/** \file aspa_piecewise.c
    \brief C function dealing with piecewise functions

    A piecewise function is defined by a set of 'nodes' or 'breakpoints'
    together with a set of values taken by the function _at_ the node
    and on the right side of it.

    Two kind of piecewise constant functions are considered:
    - piecewise constant functions to which the structure [aspa_piecewise_cst](@ref aspa_piecewise_cst) is dedicated
    - piecewise linear functions to which the structure [aspa_piecewise_lin](@ref aspa_piecewise_lin) is dedicated

    For piecewise constant function the function value _on the left side_ of the first node is specified by
    member 'ylow' of the corresponding [aspa_piecewise_cst](@ref aspa_piecewise_cst).

    The value _on the left side_ of piecewise linear functions _is undefined_. The slope on the right side of the
    last node is specified in member 'high_slope' of the corresponding [aspa_piecewise_lin](@ref aspa_piecewise_lin).


    \author Christophe Pouzat <christophe.pouzat@parisdescartes.fr>
 */

#include "aspa.h"

/** \brief Allocates an [aspa_piecewise_cst](@ref aspa_piecewise_cst).

    Member 'ylow' is initialized to 'GSL_NEGINF'.

    \returns A pointer to an allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).
 */
aspa_piecewise_cst * aspa_piecewise_cst_alloc(size_t n_nodes /**< Number of nodes.*/)
{
  aspa_piecewise_cst * pw = malloc(sizeof(aspa_piecewise_cst));
  pw->n_nodes = n_nodes;
  pw->ylow = GSL_NEGINF;
  pw->nodes = malloc(n_nodes*sizeof(double));
  pw->y = malloc(n_nodes*sizeof(double));
  return pw;
}

/** \brief Frees memory taken by an [aspa_piecewise_cst](@ref aspa_piecewise_cst)

    \returns 0 if everything goes fine.
 */
int aspa_piecewise_cst_free(aspa_piecewise_cst * pw /**< An allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/)
{
  free(pw->nodes);
  free(pw->y);
  free(pw);
  return 0;
}

/** \brief Set 'ylow' member value of an [aspa_piecewise_cst](@ref aspa_piecewise_cst)

    \returns 0 if everything goes fine.
 */
int aspa_piecewise_cst_set_ylow(aspa_piecewise_cst * pw, /**< An allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/
				double ylow /**< The new value of 'ylow'.*/)
{
  pw->ylow = ylow;
  return 0;
}

/** \brief Copies values of argument 'nodes' into member 'nodes' of
    an allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).

    The values copied into member 'nodes' are sorted in ascending order.
    If two nodes values are indentical a message is printed to the 
    'stderr' and -1 is returned.

    \returns 0 if everything goes fine, -1 otherwise.
 */
int aspa_piecewise_cst_set_nodes(aspa_piecewise_cst * pw, /**< An allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/
				 const double * nodes /**< Array of nodes values should be at least of length 'pw->n_nodes'.*/)
{
  for (size_t i=0; i < pw->n_nodes; i++)
    pw->nodes[i] = nodes[i];
  gsl_sort(pw->nodes,1,pw->n_nodes);
  for (size_t i=1; i < pw->n_nodes; i++)
    if (pw->nodes[i] == pw->nodes[i-1]) {
      fprintf(stderr,"Indentical nodes specifications are not allowed!\n");
      return -1;
    }
  return 0;
}

/** \brief Copies values of argument 'y' into member 'y' of
    an allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).

    \returns 0 if everything goes fine.
 */
int aspa_piecewise_cst_set_y(aspa_piecewise_cst * pw, /**< An allocated [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/
			     const double * y /**< Array of y values should be at least of length 'pw->n_y'.*/)
{
  for (size_t i=0; i < pw->n_nodes; i++)
    pw->y[i] = y[i];
  return 0;
}

/** \brief Returns value of an [aspa_piecewise_cst](@ref aspa_piecewise_cst)
    at x.

    The function values at the left of the first node is given by member 'ylow'.
 
    \returns The value of the piecewise function specified by 'pw' at 'x'.  
 */
double aspa_piecewise_cst_value(const aspa_piecewise_cst * pw, /**< An [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/
				double x /**< Function's argument.*/)
{
  size_t n = pw->n_nodes;
  if (x < pw->nodes[0]) return pw->ylow;
  if (x >= pw->nodes[n-1]) return pw->y[n-1];
  
  int i=0;
  int j=n-1;
  int ij;

  /* find the correct interval by bisection */
    while(i < j - 1) { 
	ij = (i + j)/2; 
	if(x < pw->nodes[ij]) j = ij; else i = ij;
	/* still i < j */
    }

    if(x == pw->nodes[j]) return pw->y[j];
    //if (gsl_fcmp(x, pw->nodes[j], 1e-10)) return pw->y[j];
    return pw->y[i];
    
}

/** \brief Allocates an [aspa_piecewise_lin](@ref aspa_piecewise_lin).

    Member 'high_slope' is initialized to 0.

    \returns A pointer to an allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).
 */
aspa_piecewise_lin * aspa_piecewise_lin_alloc(size_t n_nodes /**< Number of nodes.*/)
{
  aspa_piecewise_lin * pw = malloc(sizeof(aspa_piecewise_lin));
  pw->n_nodes = n_nodes;
  pw->high_slope = 0;
  pw->nodes = malloc(n_nodes*sizeof(double));
  pw->y = malloc(n_nodes*sizeof(double));
  return pw;
}

/** \brief Frees memory taken by an [aspa_piecewise_lin](@ref aspa_piecewise_lin)

    \returns 0 if everything goes fine.
 */
int aspa_piecewise_lin_free(aspa_piecewise_lin * pw /**< An allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).*/)
{
  free(pw->nodes);
  free(pw->y);
  free(pw);
  return 0;
}

/** \brief Set 'high_slope' member value of an [aspa_piecewise_lin](@ref aspa_piecewise_lin)

    \returns 0 if everything goes fine.
 */
int aspa_piecewise_lin_set_high_slope(aspa_piecewise_lin * pw, /**< An allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).*/
				      double high_slope /**< The new value of 'high_slope'.*/)
{
  pw->high_slope = high_slope;
  return 0;
}

/** \brief Copies values of argument 'nodes' into member 'nodes' of
    an allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).

    The values copied into member 'nodes' are sorted in ascending order.
    If two nodes values are indentical a message is printed to the 
    'stderr' and -1 is returned.

    \returns 0 if everything goes fine, -1 otherwise.
 */
int aspa_piecewise_lin_set_nodes(aspa_piecewise_lin * pw, /**< An allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).*/
				 const double * nodes /**< Array of nodes values should be at least of length 'pw->n_nodes'.*/)
{
  for (size_t i=0; i < pw->n_nodes; i++)
    pw->nodes[i] = nodes[i];
  gsl_sort(pw->nodes,1,pw->n_nodes);
  for (size_t i=1; i < pw->n_nodes; i++)
    if (pw->nodes[i] == pw->nodes[i-1]) {
      fprintf(stderr,"Indentical nodes specifications are not allowed!\n");
      return -1;
    }
  return 0;
}

/** \brief Copies values of argument 'y' into member 'y' of
    an allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).

    \returns 0 if everything goes fine.
 */
int aspa_piecewise_lin_set_y(aspa_piecewise_lin * pw, /**< An allocated [aspa_piecewise_lin](@ref aspa_piecewise_lin).*/
			     const double * y /**< Array of y values should be at least of length 'pw->n_y'.*/)
{
  for (size_t i=0; i < pw->n_nodes; i++)
    pw->y[i] = y[i];
  return 0;
}

/** \brief Returns value of an [aspa_piecewise_lin](@ref aspa_piecewise_lin)
    at x.

    The function values at the right of the last node is obtained by a 
    linear extrapolation using member 'high_slope'. The value on the left
    of the first node is not defined ('nan').
 
    \returns The value of the piecewise linear function specified by 'pw' at 'x'.  
 */
double aspa_piecewise_lin_value(const aspa_piecewise_lin * pw, /**< An [aspa_piecewise_lin](@ref aspa_piecewise_lin).*/
				double x /**< Function's argument.*/)
{
  size_t n = pw->n_nodes;
  if (x < pw->nodes[0]) return GSL_NAN;
  if (x >= pw->nodes[n-1]) return pw->y[n-1]+(x-pw->nodes[n-1])*pw->high_slope;
  
  int i=0;
  int j=n-1;
  int ij;

  /* find the correct interval by bisection */
    while(i < j - 1) { 
	ij = (i + j)/2; 
	if(x < pw->nodes[ij]) j = ij; else i = ij;
	/* still i < j */
    }

    if(x == pw->nodes[j]) return pw->y[j];
    return pw->y[i]+(pw->y[i+1]-pw->y[i])/(pw->nodes[i+1]-pw->nodes[i])*(x-pw->nodes[i]);
    
}

/** \brief Returns the integrale of an [aspa_piecewise_cst](@ref aspa_piecewise_cst).

    \returns A [aspa_piecewise_lin](@ref aspa_piecewise_lin).  
 */
aspa_piecewise_lin * aspa_piecewise_cst_integrate(const aspa_piecewise_cst * pw /**< An [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/)
{
  size_t n = pw->n_nodes;
  aspa_piecewise_lin * ipw = aspa_piecewise_lin_alloc(n);
  aspa_piecewise_lin_set_high_slope(ipw,pw->y[n-1]);
  aspa_piecewise_lin_set_nodes(ipw,pw->nodes);
  ipw->y[0] = pw->ylow;
  for (size_t i=1; i < n; i++) {
    double dx = ipw->nodes[i]-ipw->nodes[i-1];
    ipw->y[i] = pw->y[i-1]*dx+ipw->y[i-1];
  }
  return ipw;
}

/** \brief Adds two [aspa_piecewise_cst](@ref aspa_piecewise_cst) functions

    \returns A newly allocated and initialized [aspa_piecewise_cst](@ref aspa_piecewise_cst).
 */
aspa_piecewise_cst * aspa_piecewise_cst_add(const aspa_piecewise_cst * pw1, /**< [in] An [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/
					    const aspa_piecewise_cst * pw2 /**< [in] Another [aspa_piecewise_cst](@ref aspa_piecewise_cst).*/)
{
  size_t n1 = pw1->n_nodes;
  size_t n2 = pw2->n_nodes;
  double * new_nodes = malloc(sizeof(double)*(n1+n2));
  size_t node_idx;
  for (node_idx=0; node_idx < n1; node_idx++)
    new_nodes[node_idx] = pw1->nodes[node_idx];
  for (; node_idx < n1+n2; node_idx++)
    new_nodes[node_idx] = pw2->nodes[node_idx-n1];
  gsl_sort(new_nodes,1,n1+n2);
  double * new_nodes2 = malloc(sizeof(double)*(n1+n2));
  new_nodes2[0] = new_nodes[0];
  node_idx = 1;
  for (size_t i=1; i < n1+n2; i++) {
    /*Make sure that all the nodes are "different"*/
    if (gsl_fcmp(new_nodes[i],new_nodes[i-1],ASPA_EPSILON_TIME) == 1) {
      new_nodes2[node_idx] = new_nodes[i];
      node_idx++;
    }
  }
  new_nodes2 = realloc(new_nodes2,node_idx*sizeof(double));
  double *new_y = malloc(node_idx*sizeof(double));
  for (size_t i=0; i < node_idx; i++) {
    double x = new_nodes2[i];
    new_y[i] = aspa_piecewise_cst_value(pw1, x)+aspa_piecewise_cst_value(pw2, x);
  }
  free(new_nodes);
  double ylow = pw1->ylow + pw2->ylow;
  aspa_piecewise_cst * res = aspa_piecewise_cst_alloc(node_idx);
  aspa_piecewise_cst_set_ylow(res, ylow);
  aspa_piecewise_cst_set_nodes(res, new_nodes2);
  aspa_piecewise_cst_set_y(res, new_y);
  free(new_nodes2);
  free(new_y);
  return res;
}
