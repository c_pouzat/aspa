/** \file aspa_piecewise_test01.c
    \brief Test basic functions defined in file aspa_piecewise.c

    It makes sense to redirect the program output to a file:

        ./aspa_piecewise_test01 > toto
    
    and then to use gnuplot to look at the results, eg:

        plot 'toto' index 0 u 1:2 with steps
    	plot 'toto' index 1 u 1:2 with lines

    or:
   
	plot 'toto' index 2 u 1:2 with steps
	plot 'toto' index 3 u 1:2 with steps

    One has to be careful with the representation of decimal numbers
    here. In the breakpoints sequence of the second piecewise constant
    function used in this test, we see values like 1.1, 3.1, etc that
    cannot be represented exactly. That's one when the output is genera-
    ted, a given time together with this time +/- 1e-6 are used.
    \author Christophe Pouzat <christophe.pouzat@parisdescartes.fr>
 */

#include "aspa.h"

const double the_nodes[] = {1.0,2.0,3.0,4.0,5.0};
const double the_heights[] = {3.5,2.5,0.0,5.1,3.25};

const double the_nodes2[] = {1.1,2.0,3.1,4.2,5.2,6.0};
const double the_heights2[] = {-3.5,2.5,1.0,-5.2,2.0,4.0};

int main()
{
  aspa_piecewise_cst * pw = aspa_piecewise_cst_alloc(5);
  aspa_piecewise_cst_set_ylow(pw,0.0);
  aspa_piecewise_cst_set_nodes(pw,the_nodes);
  aspa_piecewise_cst_set_y(pw,the_heights);
  printf("# Working with a piecewise constant function.\n");
  printf("# x\ty\n");
  for (double x=0.5; x < 5.5; x += 0.25) {
    printf("%g\t%g\n",x-1e-6,aspa_piecewise_cst_value(pw,x-1e-6));
    printf("%g\t%g\n",x,aspa_piecewise_cst_value(pw,x));
    printf("%g\t%g\n",x+1e-6,aspa_piecewise_cst_value(pw,x+1e-6));
  }
  printf("\n\n# Constructing now the integral.\n");
  aspa_piecewise_lin * ipw = aspa_piecewise_cst_integrate(pw);
  printf("# The integral values are:\n");
  printf("# x\ty\n");
  for (double x=0; x < 7.5; x += 0.1) {
    printf("%g\t%g\n",x-1e-6,aspa_piecewise_lin_value(ipw,x-1e-6));
    printf("%g\t%g\n",x,aspa_piecewise_lin_value(ipw,x));
    printf("%g\t%g\n",x+1e-6,aspa_piecewise_lin_value(ipw,x+1e-6));
  }
  aspa_piecewise_cst * pw2 = aspa_piecewise_cst_alloc(6);
  aspa_piecewise_cst_set_ylow(pw2,0.0);
  aspa_piecewise_cst_set_nodes(pw2,the_nodes2);
  aspa_piecewise_cst_set_y(pw2,the_heights2);
  printf("\n\n# Adding to the previous piecewise constant function the following one:\n");
  printf("# x\ty\n");
  for (double x=0; x < 7.5; x += 0.1) {
    printf("%g\t%g\n",x-1e-6,aspa_piecewise_cst_value(pw2,x-1e-6));
    printf("%g\t%g\n",x,aspa_piecewise_cst_value(pw2,x));
    printf("%g\t%g\n",x+1e-6,aspa_piecewise_cst_value(pw2,x+1e-6));
  }
  aspa_piecewise_cst * pw3 = aspa_piecewise_cst_add(pw,pw2);
  printf("\n\n# The result is:\n");
  printf("# x\ty\n");
  for (double x=0; x < 7.5; x += 0.1) {
    printf("%g\t%g\n",x-1e-6,aspa_piecewise_cst_value(pw3,x-1e-6));
    printf("%g\t%g\n",x,aspa_piecewise_cst_value(pw3,x));
    printf("%g\t%g\n",x+1e-6,aspa_piecewise_cst_value(pw3,x+1e-6));
  }
  aspa_piecewise_cst_free(pw);
  aspa_piecewise_cst_free(pw2);
  aspa_piecewise_cst_free(pw3);
  aspa_piecewise_lin_free(ipw);
  exit (EXIT_SUCCESS);
}
