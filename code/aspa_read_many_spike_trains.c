/** @file aspa_read_many_spike_trains.c
 *  @brief User program for reading spike trains from several neurons
 *         at once
 *
 *  @author Christophe Pouzat <christophe.pouzat@parisdescartes.fr>
*/

#include "aspa.h"

#include <getopt.h>

static char usage[] = \
  "usage: %s [-a --sample2second=real] [-o --out-bin] ...\n"
  "          ... [-t --trial_duration=real] [-u --stim_onset=real] ...\n"
  "          ... [-d --stim_offset=real] [-i --inter_trial_interval=real] ...\n"
  "          ... -p --prefix=string -n --neurons=integer,integer ...\n"
  "          ... [-s --suffix=string]\n\n"
  "  -a --sample2second <positive real>: the factor by which times\n"
  "     in input data are divided in order get spike times in seconds\n"
  "     (default 15000).\n"
  "  -o --out-bin: if set the output is in binary format.\n"
  "  -t --trial_duration <positive real>: the inter trial interval (in s).\n"
  "     The default is set at 29.\n"
  "  -u --stim_onset <real>: the stimulus onset time (in s) if that makes sense.\n"
  "     Default set at 0.\n"
  "  -d --stim_offset <real>: the stimulus offset time (in s) if that makes sense.\n"
  "     Default set at 0.\n"
  "  -r --inter_trial_interval <positive real>: as it name says (in s),\n"
  "     default set at 30.\n"
  "  -p --prefix <string>: The string making the 'prefix' of the names of\n"
  "     the files containing the spike trains.\n"
  "  -n --neurons <integer,integer>: comma separated list of indexes use to\n"
  "     distinguish the data file names.\n"
  "  -s --suffix <string>: the string making the suffix of the data file names\n\n"
  " The program opens a sequence of files whose names are built from:\n"
  "  'prefix'+number+'suffix',\n"
  " where 'prefix' and 'suffix' are given by the parameters with the same names\n"
  " and number takes the successive values specified in parameter 'neuron'.\n"
  " It makes a proper aspa_msta structure out of these data and prints them\n"
  " to the 'stdout' in text format (by default) or in binary format if 'out_bin'\n"
  " is selected.\n"
  " The 'consistency' of the data from the different neurons (same number of trials,\n"
  " same stimulus onset and offset, same trial duration) is checked and a message is\n"
  " printed to the 'stderr' together with some 'general' information.\n"
  "\n";


/** @brief Reads command line arguments.
 *  
 *  @return 0 when everything goes fine
*/
int read_args(int argc, /**< [in] Argument of main.*/
	      char ** argv, /**<  [in] Argument of main.*/
	      double * inter_trial_interval, /**<  [out] The inter trial interval (in s).*/
	      double * stim_onset, /**< [out] Stimulus onset (in s), can do without, defaut 0.*/
	      double * stim_offset, /**< [out] Stimulus offset (in s), can do without, defaut 0.*/
	      double * trial_duration, /**< [out] The duration of individual trials (in s).*/
	      double * sample2second, /**< [out] Sample to second correction 
					 (sampling rate if data not already in s, default 15000).*/
	      char * prefix, /**< [out] The prefix of the data file names.*/
	      char * suffix, /**<  [out] The suffix of the data file names.*/
	      int * neurons, /**< [out] The indexes of the neurons to consider (an array).*/
	      size_t * nb_neurons, /**<  [out] The number of the neurons to consider.*/
	      int * out_bin /**< [out] Indicator of the type of output to produce (0
			     for text and 1 for binary).*/)
{
  // Define default values
  *stim_onset=0;
  *stim_offset=0;
  *sample2second=15000.0;
  *trial_duration=29.0;
  *inter_trial_interval=30.0;
  *out_bin=0;
  size_t nb_neurons_true=0;
  {int opt;
    static struct option long_options[] = {
      {"sample2second",optional_argument,NULL,'a'},
      {"out_bin",optional_argument,NULL,'o'},
      {"trial_duration",optional_argument,NULL,'t'},
      {"inter_trial_interval",optional_argument,NULL,'r'},
      {"stim_onset",optional_argument,NULL,'u'},
      {"stim_offset",optional_argument,NULL,'d'},
      {"prefix",required_argument,NULL,'p'},
      {"suffix",required_argument,NULL,'s'},
      {"neurons",required_argument,NULL,'n'},
      {"help",no_argument,NULL,'h'},
      {NULL,0,NULL,0}
    };
    int long_index =0;
    while ((opt = getopt_long(argc,argv,"a:s:t:u:d:r:p:n:s:ho",long_options,\
			      &long_index)) != -1) {
      switch(opt) {
      case 'o':
      {
	*out_bin=1;
      }
      break;
      case 'p':
      {
	strcpy(prefix,optarg);
      }
      break;
      case 's':
      {
	strcpy(suffix,optarg);
      }
      break;
      case 'n':
      {
        char *start = strdup(optarg); // duplicate optarg content
        char *running;
        running = start;
        char *token  = strsep(&running, ",");
        while (token != NULL) {
          token = strsep (&running, ","); // split optarg at each ","
          nb_neurons_true++;
        }
        free(start);
        // The number of neurons is now known
        // Allocate memory for the vector of neuron
        // indexes
	neurons = realloc(neurons,nb_neurons_true*sizeof(int));
	*nb_neurons = nb_neurons_true;
        start = strdup(optarg); // duplicate optarg content again
        running = start;
        // Get the index of each stimulation
        for (size_t i=0; i<(*nb_neurons); i++) {
          token = strsep (&running, ",");
          neurons[i] = atoi(token);
        }
        free(start);
      }
      break;
      case 'a':
      {
	float a=atof(optarg);
	if (a <= 0)
	{
	  fprintf(stderr,"The sample to seconds conversion factor should be > 0.\n");
	  return -1;
	}
	*sample2second = (double) a; 
      }
      break;
      case 't':
      {
	float t=atof(optarg);
	if (t <= 0)
	{
	  fprintf(stderr,"Trial duration should be > 0.\n");
	  return -1;
	}
	*trial_duration = (double) t; 
      }
      break;
      case 'r':
      {
	float r=atof(optarg);
	if (r <= 0)
	{
	  fprintf(stderr,"The inter trial interval should be > 0.\n");
	  return -1;
	}
	*inter_trial_interval = (double) r; 
      }
      break;
      case 'u':
      {
	float u=atof(optarg);
	*stim_onset = (double) u; 
      }
      break;
      case 'd':
      {
	float d=atof(optarg);
	*stim_offset = (double) d; 
      }
      break;
      case 'h': printf(usage,argv[0]);
	return -1;
      default : fprintf(stderr,argv[0]);
	return -1;
      }
    }
  }
  if (*trial_duration > *inter_trial_interval)
  {
    fprintf(stderr,"Trial duration cannot be larger than inter trial interval.\n");
    return -1;
  }
  if (*stim_offset < *stim_onset)
  {
    fprintf(stderr,"Stim offset must be larger than stim onset.\n");
    return -1;
  }
  return 0;
}

int main(int argc, char ** argv)
{
  double inter_trial_interval=0;
  double stim_onset,stim_offset,sample2second;
  double trial_duration;
  size_t nb_neurons=100;
  int out_bin=0;
  int *neurons = malloc(nb_neurons*sizeof(int));
  char prefix[512];
  char suffix[512];
  int status = read_args(argc,argv,&inter_trial_interval,
			 &stim_onset,&stim_offset,&trial_duration,
			 &sample2second,prefix,suffix,
			 neurons,&nb_neurons,&out_bin);
  if (status == -1) exit (EXIT_FAILURE);
  fprintf(stderr,"%ld neurons are considered.\n",nb_neurons);
  aspa_msta * msta = aspa_msta_alloc(nb_neurons);
  for (size_t n_idx=0; n_idx < nb_neurons; n_idx++) {
    char data_file[2048];
    strcpy(data_file,prefix);
    char number[1024];
    sprintf(number,"%d",neurons[n_idx]);
    strcat(data_file,number);
    strcat(data_file,suffix);
    FILE * fp = fopen(data_file,"r");
    gsl_vector * st_flat = aspa_raw_fscanf(fp,sample2second);
    fclose(fp);
    fprintf(stderr,"Data from neuron %ld in file %s have been read.\n",
	    n_idx,data_file);
    msta->sta[n_idx] = aspa_sta_from_raw(st_flat, inter_trial_interval,
					 stim_onset, stim_offset,
					 trial_duration);
    gsl_vector_free(st_flat);
  }
  free(neurons);
  status = aspa_msta_check(msta);
  if (status == 0)
    fprintf(stderr,"The data from the different neurons are consistent.\n");
  else
    fprintf(stderr,"The data from the different neurons are not consistent.\n");
  if (out_bin == 0) {
    fprintf(stderr,"The output is written in text format.\n\n");
    aspa_msta_fprintf(stdout,msta);
  } else {
    fprintf(stderr,"The output is written in binary format.\n\n");
    aspa_msta_fwrite(stdout,msta);
  }
  aspa_msta_free(msta);
  exit (EXIT_SUCCESS);
}
