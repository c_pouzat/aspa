## @package aspa
#  Module for spike train analysis ("Analyse des séquences de
#  potentiels d'action"--aspa--in French).

import numpy as np
import matplotlib.pyplot as plt

def Kolmogorov_D(Up,
                 what="D"):
    """Returns the Kolmogorov two sided or one sided statistics against
       the null hypothesis---uniform distribution on [0,1]
    
    Parameters
    ----------
    Up: A 1D array or a list containing the sample
    what: One of "D", "D+", "D-" for two sided or one of the onse sided tests
          (default set to "D").

    Returns
    -------
    A scalar, the value of the statistics.
    """
    if not np.all(Up > 0) and np.all(Up < 1):
        raise ValueError('Every u in Up must satisfy 0 < u < 1')
    if not what in ["D","D+","D-"]:
        raise ValueError('what must be one of "D","D+","D-"')
    n = len(Up)
    ecdf = np.arange(1,n+1)/n
    Up = np.sort(Up)
    Dp = np.max(ecdf-Up)*np.sqrt(n)
    Dm = np.max(Up[:-1]-ecdf[:-1]+1/n)*np.sqrt(n)
    if what == "D": return max(Dp,Dm)
    if what == "D+": return Dp
    if what == "D-": return Dm


def pDsN(z,k_max=9):
    """Returns the asymptotic value of CDF of the Kolmogorov
    D_n*\sqrt(n) statistics

    Parameters
    ----------
    z: The Kolmogorov statistics D_n*\sqrt(n)
    k: A positive integer the largest term of the
       series (default set to 9).

    Returns
    -------
    The probability of event K <= D_n*\sqrt(n)
    """
    partial_sum = np.cumsum((-1)**np.arange(1,k_max+1)*np.exp(-2*np.arange(1,k_max+1)**2*z**2))
    for i in range(1,int(round(k_max*2/3))):
        partial_sum = (partial_sum[1:]+partial_sum[:-1])/2
    return 1+2*partial_sum[-1]


def AndersonDarling_W2(Up):
    """Returns the Anderson-Darling statistic

    Parameters
    ----------
    Up: A list of 1D array with the sample

    Returns
    -------
    The Anderson-Darling statistic
    """
    if not np.all(Up > 0) and np.all(Up < 1):
        raise ValueError('Every u in Up must satisfy 0 < u < 1')
    n = len(Up)
    return -n-np.sum((2*np.arange(1,n+1)-1)*np.log(Up)+
                     (2*n-2*np.arange(1,n+1)+1)*np.log(1-Up))/n


def pAD_W2(x):
    """Returns the probability of the Anderson-Darling statistic

    The algorithm is the one of Marsaglia & Marsaglia (2004)
    Evaluating the Anderson-Darling Distribution
    Journal of Statistical Software 9(2):1-5.

    Parameters
    ----------
    x: The Anderson-Darling statistic

    Returns
    -------
    The probability of events AD <= x
    """
    if x<=0: return 0
    if 0 < x < 2:
        res = 1/np.sqrt(x)*np.exp(-1.2337141/x)
        res *= (2.00012+
                (.247105-
                 (.0649821-
                  (.0347962-
                   (.011672-.00168691*x)*x)*x)*x)*x)
        return res
    res = 1.0776-(2.30695-
                  (.43424-
                   (.082433-
                    (.008056-.0003146*x)*x)*x)*x)*x
    res = np.exp(-np.exp(res))
    return res


def DurbinTransform(observed_times,
                    observation_interval=None):
    """Performs a Durbin's transformation of the data

    The method is described in:
    Durbin, J. (1961) Some Methods of Constructing Exact Tests
    Biometrika 48(1/2):41-55.

    Parameters
    ----------
    observed_times: A list or 1D array with observations
    observation_interval: An optional two elements list or 1D array
      with the range of the observations. If not specified the floor
      of the minimum and the ceil of the maximum are used.

    Returns
    -------
    The Durbin transformation of observed_times (a 1D array) 
    """
    if observation_interval == None:
        observation_interval = [np.floor(np.min(observed_times)),
                                np.ceil(np.max(observed_times))]
    if not np.all(np.logical_and(observation_interval[0] < observed_times,
                                 observed_times < observation_interval[1])):
        raise ValueError('observation_interval is not compatible with'+\
                         'observed_times')
    observed_times = observed_times.copy()-observation_interval[0]
    obs_duration = np.diff(observation_interval)
    n = len(observed_times)
    observed_times /= obs_duration
    iei = [observed_times[0]]+\
          list(np.sort(np.diff(observed_times)))+\
          [1-observed_times[-1]]
    siei = [0]+sorted(iei)
    g = (n+2-np.arange(1,n+2))*np.diff(siei)
    return np.cumsum(g[:-1])

def discretize_time(observed_times,
                    observation_period=[0,6],
                    sampling_period=1/12800):
    """Forces continuous times into discrete time bins
    
    This function is meant to be used for exploring the effect
    of time discretization on spike trains

    Parameters
    ----------
    observed_times: A list or 1D array with the data
    observation_period: A 2 elements list or vector with the time
      at which the observations started and stoped
    sampling_period: A positive real with the sampling period used
      to discretize the data

    Results
    -------
    The discretized version of observed_times
    """
    dt = np.arange(observation_period[0],
                   observation_period[1],
                   sampling_period)
    return (0.5+(np.digitize(observed_times,dt)-1))*sampling_period

def jitter_time(observed_times,
                observation_period=[0,6],
                sampling_period=1/12800):
    """Introduce uniform jitter over a sampling period for
    discretized data.

    observed_times: A list or 1D array with the discretized data
    observation_period: A 2 elements list or vector with the time
      at which the observations started and stoped
    sampling_period: A positive real with the sampling period used
      to discretize the data

    Results
    -------
    The jittered version of observed_times
    """
    import numpy as np
    from numpy.random import random_sample
    n = len(observed_times)
    res = np.zeros(n)
    within = np.logical_and(observation_period[0]+\
                            sampling_period/2 < observed_times,
                            observed_times < observation_period[1]-\
                            sampling_period/2)
    res[within] = observed_times[within]+\
      (random_sample(sum(within))-0.5)*sampling_period
    too_small = observation_period[0]+sampling_period/2 >= observed_times
    if sum(too_small) > 0:
        s = random_sample(sum(too_small))
        s *= (observed_times[too_small]+sampling_period/2-\
              observation_period[0]*np.ones(len(s)))
        s += observation_period[0]*np.ones(len(s))
        res[too_small] = s	
    too_big = observed_times >= observation_period[1]-sampling_period/2
    if sum(too_big)>0:
        b = random_sample(sum(too_big))
        b *= (observation_period[1]*np.ones(len(b))-\
              observed_times[too_big]-sampling_period/2)
        b += observed_times[too_big]-sampling_period/2
        res[too_big] = b
    res[res==0] = 5*np.finfo(float).eps
    return np.sort(res)

class StabilizedPSTH:
    """Holds a Peri-Stimuls Time Histogram (PSTH) and
    its variance stabilized version.

    Attributes:
        st (1d array): aggregated spike trains (stimulus on at 0).
        x (1d array): bins' centers.
        y (1d array): stabilized counts.
        n (1d array): actual counts.
        n_stim (scalar): number of trials used to build
            the PSTH.
        width (scalar): bin width.
        stab_method (string): variance stabilization method.
        spontaneous_rate (scalar): spontaneous rate.
        support_length (scalar): length of the PSTH support.
    """
    def __init__(self,spike_train_list,
                 onset,region = [-2,8],
                 spontaneous_rate = None,target_mean = 3,
                 stab_method = "Freeman-Tukey"):
        """ Create a StabilizedPSTH instance.
        
        Parameters
        ----------
        spike_train_list: a list of spike trains (vectors with strictly
          increasing elements), where each element of the list is supposed
          to contain a response and where each list element is assumed
          time locked to a common reference time.
        onset: a number giving to the onset time of the stimulus.
        region: a two components list with the number of seconds before
          the onset (a negative number typically) and the number of second
          after the onset one wants to use for the analysis.
        spontaneous_rate: a positive number with the spontaneous rate
          assumed measured separately; if None, the overall rate obtained
          from spike_train_list is used; the parameter is used to set the
          bin width automatically.
        target_mean: a positive number, the desired mean number of events
          per bin under the assumption of homogeneity.
        stab_method: a string, either "Freeman-Tukey" (the default,
          x -> sqrt(x)+sqrt(x+1)), "Anscombe" (x -> 2*sqrt(x+3/8)) or "Brown
          et al" (x -> 2*sqrt(x+1/4); the variance stabilizing transformation.
        """
        if not isinstance(spike_train_list,list):
            raise TypeError('spike_train_list must be a list')
        n_stim = len(spike_train_list)
        aggregated = np.sort(np.concatenate(spike_train_list))
        if spontaneous_rate is None:
            time_span = np.ceil(aggregated[-1])-np.floor(aggregated[0])
            spontaneous_rate = len(aggregated)/n_stim/time_span
        if not spontaneous_rate > 0:
            raise ValueError('spontaneous_rate must be positive')
        if not stab_method in ["Freeman-Tukey","Anscombe","Brown et al"]:
            raise ValueError('stab_method should be one of '\
                             +'"Freeman-Tukey","Anscombe","Brown et al"')
        left = region[0]+onset
        right = region[1]+onset
        aggregated = aggregated[np.logical_and(left <= aggregated,
                                               aggregated <= right)]-onset
        bin_width = np.ceil(target_mean/n_stim/spontaneous_rate*1000)/1000
        aggregated_bin = np.arange(region[0],
                                   region[1]+bin_width,
                                   bin_width)
        aggregated_counts, aggregated_bin = np.histogram(aggregated,
                                                         aggregated_bin)
        if stab_method == "Freeman-Tukey":
            y = np.sqrt(aggregated_counts)+np.sqrt(aggregated_counts+1)
        elif stab_method == "Anscombe":
            y = 2*np.sqrt(aggregated_counts+0.375)
        else:
            y = 2*np.sqrt(aggregated_counts+0.25)
        self.st = aggregated
        self.x = aggregated_bin[:-1]+bin_width/2
        self.y = y
        self.n = aggregated_counts
        self.n_stim = n_stim
        self.width = bin_width
        self.stab_method = stab_method
        self.spontaneous_rate = spontaneous_rate
        self.support_length = np.diff(region)[0]
    def __str__(self):
        """Controls the printed version of the instance."""
        import numpy as np
        return "An instance of StabilizedPSTH built from " \
            + str(self.n_stim) + " trials with a " + str(self.width) \
            + " (s) bin width.\n  The PSTH is defined on a domain " \
            + str(self.support_length) + " s long.\n" \
            + "  The stimulus comes at second 0.\n" \
            + "  Variance was stabilized with the " \
            + self.stab_method + " method.\n"
    def plot(self,
             what="stab",
             linewidth=1,
             color='black',
             xlabel="Time (s)",
             ylabel=None):
            """Plot the data.
            
            Parameters
            ----------
            what: a string, either 'stab' (to plot the stabilized version) 
                  or 'counts' (to plot the actual counts).
            The other parameters (linewidth,color,xlabel,ylabel) have their 
                classical meaning      
            """
            if not what in ["stab","counts"]:
                raise ValueError('what should be either "stab" or "counts"')
            if what == "stab":
                y = self.y
                if ylabel is None:
                    if self.stab_method == "Freeman-Tukey":
                        ylabel = r'$\sqrt{n}+\sqrt{n+1}$'
                    elif self.stab_method == "Anscombe":
                        ylabel = r'$2 \sqrt{n+3/8}$'
                    else:
                        ylabel = r'$2 \sqrt{n+1/4}$'
            else:
                y = self.n
                if ylabel is None:
                    ylabel = "Counts per bin"
            plt.plot(self.x,y,color=color,linewidth=linewidth)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)

def tricube_kernel(x,bw=1.0):
    """Returns the value of a tricube kernel at x.

    Parameters
    ----------
    x: argument (a real)
    bw: the bin width a positive real (default set to 1)

    Results
    -------
    The value of the tricube kernel at x;
    """
    ax = np.absolute(x/bw)
    result = np.zeros(x.shape)
    result[ax <= 1] = 70*(1-ax[ax <= 1]**3)**3/81.
    return result

def NW_Estimator(x,X,Y,
                 kernel = lambda y: tricube_kernel(y,1.0)):
    """Returns the Nadaraya-Watson estimator at x, given data X and Y
    using kernel.

    Parameters
    ----------
    x: point at which the estimator is looked for.
    X: abscissa of the observations.
    Y: ordinates of the observations.
    kernel: a univariate 'weight' function.

    Returns
    -------
    The estimated ordinate at x.
    """
    w = kernel(X-x)
    return np.sum(w*Y)/np.sum(w)

def Cp_score(X,Y,bw = 1.0, kernel = tricube_kernel,sigma2=1):
    """Computes Mallow's Cp score given data X and Y, a bandwidth bw,
    a bivariate function kernel and a variance sigma2.

    Parameters
    ----------
    X: abscissa of the observations.
    Y: ordinates of the observations.
    bw: the bandwidth.
    kernel: a bivariate function taking an ordinate as first parameter
            and a bandwidth as second parameter.
    sigma2: the variance of the ordinates.

    Returns
    -------
    A tuple with the bandwidth, the trace of the smoother and the 
    Cp score.
    """
    from numpy.matlib import identity
    L = np.zeros((len(X),len(X)))
    ligne = np.zeros(len(X))
    for i in range(len(X)):
        ligne = kernel(X-X[i], bw)
        L[i,:] = ligne/np.sum(ligne)
    n = len(X)
    trace = np.trace(L)
    if trace == n: return None
    Cp = np.dot(np.dot(Y,(identity(n)-L)),
                np.dot((identity(n)-L),Y).T)[0,0]/n + 2*sigma2*trace/n
    return (bw, trace, Cp)

def tube_target(x,alpha,kappa):
    """Function used to get the width of the confidence band.

    The function returns: 2(1-Φ(x))+κ*exp(-x^2/2)/π - α
    where Φ is the normal CDF, α is the confidence level requested,
    κ is the square root of the integral 
    of the square of the kernel derivative over the definition domain:
    κ = \sqrt{\inf_a^b K'(t)^2 dt}.

    Parameters
    ----------
    x: a real.
    alpha: a real such that 0 < alpha < 1, the confidence level.
    kappa: the parameter κ

    Results
    -------
    A real number.
    """
    from scipy.stats import norm
    return 2*(1-norm.cdf(x)) + kappa*np.exp(-x**2/2)/np.pi - alpha

def make_L(X,kernel = lambda y: tricube_kernel(y,1.0)):
    """Return the smoothing matrix of a kernel based regression

    Parameters
    ----------
    X: A list of 1D array with the bin centers positions
    kernel: A function corresponding to the smoothing kernel

    Results
    -------
    A square matrix with as many rows as X as elements.
    """
    result = np.zeros((len(X),len(X)))
    ligne = np.zeros(len(X))
    for i in range(len(X)):
        ligne = kernel(X-X[i])
        result[i,:] = ligne/np.sum(ligne)
    return result

class SmoothStabilizedPSTH:
    """Holds a smooth stabilized Peri-Stimuls Time Histogram (PSTH).
    
    Attributes:
      x (1d array): bins' centers.
      y (1d array): stabilized counts.
      n (1d array): actual counts.
      n_stim (scalar): number of trials used to build
        the PSTH.
      width (scalar): bin width.
      stab_method (string): variance stabilization method.
      spontaneous_rate (scalar): spontaneous rate.
      support_length (scalar): length of the PSTH support.
      bandWidthMultipliers (1d array): the bandwidth multipliers
        considered.
      bw_values (1d array): the bandwith values considered.
      trace_values (1d array): traces of the corresponding
        smoothing matrices.
      Cp_values (1d array): Mallow's Cp values.
      bw_best_Cp (scalar): best Cp value.
      NW (1d array): Nadaraya-Watson Estimator with the best
        bandwidth.
      L_best (2d array): smoothing matrix with the best
        bandwidth.
      L_best_norm (1d array): sums of the squared rows of
        L_best.
      kappa_0 (scalar): value of kappa_0.
    """
    def __init__(self,
                 sPSTH,
                 bandWidthMultipliers = [5,10,50,100],
                 sigma2=1):
        import numpy as np
        if not isinstance(sPSTH,StabilizedPSTH):
            raise TypeError('sPSTH must be an instance of StabilizedPSTH')
        if not np.all(np.array(bandWidthMultipliers)>1):
            raise ValueError('Each element of bandWidthMultipliers must be > 1')
        if not sigma2 > 0:
            raise ValueError('sigma2 must be > 0')	     
        def tricube_kernel(x,bw=1.0):
            ax = np.absolute(x/bw)
            result = np.zeros(x.shape)
            result[ax <= 1] = 70*(1-ax[ax <= 1]**3)**3/81.
            return result
        def NW_Estimator(x,X,Y,
                         kernel = lambda y:
                         tricube_kernel(y,1.0)):
            """Returns the Nadaray-Watson estimator at x, given data X and Y
            using kernel.
        
            Parameters
            ----------
            x: point at which the estimator is looked for.
            X: abscissa of the observations.
            Y: ordinates of the observations.
            kernel: a univariate 'weight' function.
        
            Returns
            -------
            The estimated ordinate at x.
            """
            w = kernel(X-x)
            return np.sum(w*Y)/np.sum(w)
        def Cp_score(X,Y,bw = 1.0, kernel = tricube_kernel,sigma2=1):
            """Computes Mallow's Cp score given data X and Y, a bandwidth bw,
            a bivariate function kernel and a variance sigma2.
        
            Parameters
            ----------
            X: abscissa of the observations.
            Y: ordinates of the observations.
            bw: the bandwidth.
            kernel: a bivariate function taking an ordinate as first parameter
                    and a bandwidth as second parameter.
            sigma2: the variance of the ordinates.
        
            Returns
            -------
            A tuple with the bandwidth, the trace of the smoother and the 
            Cp score.
            """
            from numpy.matlib import identity
            L = np.zeros((len(X),len(X)))
            ligne = np.zeros(len(X))
            for i in range(len(X)):
                ligne = kernel(X-X[i], bw)
                L[i,:] = ligne/np.sum(ligne)
            n = len(X)
            trace = np.trace(L)
            if trace == n: return None
            Cp = np.dot(np.dot(Y,(identity(n)-L)),
                        np.dot((identity(n)-L),Y).T)[0,0]/n + 2*sigma2*trace/n
            return (bw, trace, Cp)
        def make_L(X,kernel = lambda y: tricube_kernel(y,1.0)):
            result = np.zeros((len(X),len(X)))
            ligne = np.zeros(len(X))
            for i in range(len(X)):
                ligne = kernel(X-X[i])
                result[i,:] = ligne/np.sum(ligne)
            return result 
        self.st = sPSTH.st.copy()
        self.x = sPSTH.x.copy()
        self.y = sPSTH.y.copy()
        self.n = sPSTH.n.copy()
        self.n_stim = sPSTH.n_stim
        self.width = sPSTH.width
        self.spontaneous_rate = sPSTH.spontaneous_rate
        self.stab_method = sPSTH.stab_method
        self.support_length = sPSTH.support_length
        bw_vector = self.width*np.array(bandWidthMultipliers)
        Cp_values = np.array([Cp_score(self.x,self.y,bw)
                              for bw in bw_vector])
        bw_best_Cp = bw_vector[np.argmin(Cp_values[:,2])]
        NW = np.array([NW_Estimator(x,self.x,self.y,
                                    kernel = lambda y:
                                    tricube_kernel(y,
                                                   bw_best_Cp))
                       for x in self.x])
        L_best = make_L(self.x,
                        kernel = lambda y:
                        tricube_kernel(y,bw_best_Cp))
        L_best_norm = np.sqrt(np.sum(L_best**2,axis=1))
        IK = 1.49866250530693
        kappa_0 = self.support_length*IK/bw_best_Cp
        self.bandWidthMultipliers = bandWidthMultipliers
        self.bw_values = Cp_values[:,0].copy()
        self.trace_values = Cp_values[:,1].copy()
        self.Cp_values = Cp_values[:,2].copy()
        self.bw_best_Cp = bw_best_Cp
        self.NW=NW
        self.L_best=L_best
        self.L_best_norm=L_best_norm
        self.kappa_0 = kappa_0
    def get_c(self,alpha=0.05,lower=2,upper=5):
        """Get solution of 2*(1-norm.cdf(x)) + 
        kappa*np.exp(-x**2/2)/np.pi - alpha/len(self..bw_vector).
    
        Parameters
        ----------
        alpha (0 < scalar < 1): the confidence level.
        lower (scalar > 0): the lower starting point of the Brent method.
        upper (scalar > 0): the upper starting point of the Brent method.
    
        Return
        ------
        The solution (scalar).
    
        Details
        -------
        The Brent method is used.
        A Bonferroni correction is performed.
        """
        if not 0 < alpha < 1:
            raise ValueError('alpha must be > 0 and < 1')
        if not lower > 0:
            raise ValueError('lower must be > 0')
        if not upper > 0:
            raise ValueError('upper must be > 0')
        def tube_target(x,alpha,kappa):
            from scipy.stats import norm
            return 2*(1-norm.cdf(x)) + kappa*np.exp(-x**2/2)/np.pi - alpha
        from scipy.optimize import brentq
        return brentq(tube_target,a=lower,b=upper,
                      args=(alpha/len(self.bw_values),self.kappa_0))
    def plot(self,what="band",color='black',
             alpha=0.01,lower=2,upper=6,
             ylabel=None,xlabel=None):
        import matplotlib.pyplot as plt
        if not what in ["smooth","band","stab",
                        "Cp vs bandwidth", "Cp vs trace"]:
            msg = 'what should be one of "smooth", "band", '+\
                  '"stab", "Cp vs bandwidth", "Cp vs trace"'
            raise ValueError(msg)
        if what in ["smooth","band","stab"]:
            if ylabel is None:
                if self.stab_method == "Freeman-Tukey":
                    ylabel = r'$\sqrt{n}+\sqrt{n+1}$'
                elif self.stab_method == "Anscombe":
                    ylabel = r'$2 \sqrt{n+3/8}$'
                else:
                    ylabel = r'$2 \sqrt{n+1/4}$'
            if what == "stab":
                y = self.y
            if what == "smooth":
                y = self.NW
            if what == "band":
                y = self.NW
                c = self.get_c(alpha,lower,upper)
                u = y + c*self.L_best_norm
                l = y - c*self.L_best_norm
            if xlabel is None:
                xlabel = "Time (s)"
            if what in ["stab","smooth"]:
                plt.plot(self.x,y,color=color)
            else:
                plt.fill_between(self.x,u,l,color=color)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
        else:
            y = self.Cp_values
            if ylabel is None:
                ylabel = "Cp"
            if what == "Cp vs bandwidth":
                X = self.bw_values
                if xlabel is None:
                    xlabel = "Bandwidth (s)"
            else:
                X = self.trace_values
                if xlabel is None:
                    xlabel = "Smoother trace"
            plt.plot(X,y,color=color)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
    def uplot(self,what="band",color='black',
              alpha=0.01,lower=2,upper=6,
              ylabel=None,xlabel=None):
        import matplotlib.pyplot as plt
        if not what in ["smooth","band","stab"]:
            msg = 'what should be one of "smooth", "band", "stab"'
            raise ValueError(msg)
        if ylabel is None:
            ylabel = "Frequency (Hz)"
        if xlabel is None:
            xlabel = "Time (s)"
        if what == "stab":
            y = self.y
        else:
            y = self.NW
        if what == "band":
            c = self.get_c(alpha,lower,upper)
            u = y + c*self.L_best_norm
            l = y - c*self.L_best_norm
        if self.stab_method == "Freeman-Tukey": 
            def InvFct(y):
                y = np.maximum(y,1)
                return ((y**2-1)/2./y)**2/self.n_stim/self.width
        if self.stab_method == "Anscombe":
            def InvFct(y):
                y = np.maximum(y,2*np.sqrt(3/8.))
                return (y**2/4. + np.sqrt(1.5)/4./y - 11/8./y**2 -\
                        1/8.)/self.n_stim/self.width
        if self.stab_method == "Brown et al": 
            def InvFct(y):
                y = np.maximum(y,1)
                return (y**2/4.-0.25)/self.n_stim/self.width
        y = InvFct(y)
        if what in ["stab","smooth"]:
            plt.plot(self.x,y,color=color)
        else:
            u = InvFct(u)
            l = InvFct(l)
            plt.fill_between(self.x,u,l,color=color)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

def raster_plot(train_list,
                stim_onset=None,
                color = 'black'):
    """Create a raster plot.

    Parameters
    ----------
    train_list: a list of spike trains (1d vector with strictly
      increasing elements).
    stim_onst: a number giving the time of stimulus onset. If
      specificied, the time are realigned such that the stimulus
      comes at 0.
    color: the color of the ticks representing the spikes.

    Side effect:
    A raster plot is created.
    """
    if stim_onset is None:
        stim_onset = 0
    for idx,trial in enumerate(train_list):
        plt.vlines([x-stim_onset for x in trial],
                   idx+0.6,idx+1.4,
                   color=color)
    plt.ylim(0.5,len(train_list)+1.0)
    plt.xlabel("Times (s)")
    plt.ylabel("Trial")

def cp_plot(train_list,
            what='rt',
            color = 'black',
            inter_trial_interval=30):
    """Plot the observed counting process

    Parameters
    ----------
    train_list: A list of lists with the spike trains of each trial
    what: A string 'rt', 'wt' or 'norm' (default 'rt')
    color: The color used to draw the observed counting process(es)
    inter_trial_interval: The time between successive trials

    Results
    -------
    Nothing is returned, the function is used for its side effect.
    """
    if not what in ["rt","wt","norm"]:
            msg = 'what should be one of "rt", "wt", "norm"'
            raise ValueError(msg)
    if what == "rt":
        st = train_list[0].copy
        for st_idx in range(1,len(train_list)):
            st += [x+st_idx*inter_trial_interval for x in train_list[st_idx]]
        plt.step(st,np.arange(len(st))+1,where='post',color=color)
    if what == "wt":
        for st in train_list:
            plt.step(st,np.arange(len(st))+1,where='post',
                     color=color)
    if what == "norm":
        st = train_list[0].copy()
        for i in range(1,len(train_list)):
            st += train_list[i]
        st.sort()
        plt.step(st,(np.arange(len(st))+1)/len(train_list),
                  where='post',color=color)
    plt.grid()
    plt.xlabel("Time (s)")
    plt.ylabel("Number of events")

def G_at_1_with_bounds(c_fct,b_fct,n,bounds=True):
    """Probabilty for a canonical Brownian motion to cross a boundary
    defined by the continous function c_fct beween 0 and 1.
    
    Parameters
    ----------
    c_fct: a continuous function of a single variable defining the
           boundary.
    b_fct: an accessory function helping the convergence, the
           derivative of c_fct is a good default choice.
    bounds: a Boolean variable, if True (default) lower and upper
            bounds for the probability are returned.
    
    Returns
    -------
    The probability if bounds is False or a tuple with the lower bound
    the probability and the upper bound.
    
    Details
    -------
    Bounds calculation uses Eq. 3.6 and 3.7 p 102 of Loader and Deely
    (1987) J Statist Comput Simul 27: 95-105, and some conditions on
    the partial derivative of the Kernel appearing in the Volterra
    integral equation are supposed to be met."""    
    from scipy.stats import norm
    def F(t):
        c_t = c_fct(t)
        b_t = b_fct(t)
        term1 = norm.cdf(-c_t/np.sqrt(t))
        factorA =  np.exp(-2*b_t*(c_t-t*b_t))
        factorB = norm.cdf((-c_t+2*t*b_t)/np.sqrt(t))
        return term1 + factorA*factorB
    def K(t,u):
        if t == u:
            return 1.0
        c_t = c_fct(t)
        c_u = c_fct(u)
        b_t = b_fct(t)
        term1 = norm.cdf((c_u-c_t)/np.sqrt(t-u))
        factorA =  np.exp(-2*b_t*(c_t-c_u-(t-u)*b_t))
        factorB = norm.cdf((c_u-c_t+2*(t-u)*b_t)/np.sqrt(t-u))
        return term1 + factorA*factorB
    t_v = np.linspace(0,1,n+1)
    t_v_half = (t_v[1:]+t_v[:-1])*0.5
    Delta = np.zeros((n))
    if bounds:
        G_L = np.zeros((n))
        G_U = np.zeros((n))
        G_L[0] = F(t_v[1])
        G_U[0] = F(t_v[1])/K(t_v[1],t_v[0])
    Delta[0] = F(t_v[1])/K(t_v[1],t_v_half[0])
    for j in range(1,n):
        term1 = F(t_v[j+1])
        factor1 = Delta[:j]
        factor2 = [K(t_v[j+1],t) for t in t_v_half[:j]]
        term2 = np.sum(factor1*np.array(factor2))
        divisor = K(t_v[j+1],t_v_half[j])
        Delta[j] = (term1-term2)/divisor
        if bounds:
            factor2 = np.diff(np.array([K(t_v[j+1],t)
                                        for t in t_v[:(j+2)]]))
            G_L[j] = term1 + np.sum(G_L[:j]*factor2[1:])
            G_U[j] = (term1 +
                    np.sum(G_U[:j]*factor2[:-1]))/K(t_v[j+1],t_v[j])
    if bounds:
        return (G_L[n-1],np.sum(Delta),G_U[n-1])
    else:
        return np.sum(Delta)


if __name__ == '__main__':
    print("""
    Testing now Anderson-Darling W2 distribution definition
    using the 0.90, 0.95 and 0.99 quantiles:
    """)
    print(" Nominal 0.90, computed: ",pAD_W2(1.9329578327),", difference: ",
          0.9-pAD_W2(1.9329578327),"\n",
          "nominal 0.95, computed: ",pAD_W2(2.492367),", difference: ",
          0.95-pAD_W2(2.492367),"\n",
          "nominal 0.99, computed: ",pAD_W2(3.878125),", difference: ",
          0.99-pAD_W2(3.878125))
    print("""
    Testing now the effect of time dicretization on the Kolomogorov
    and Anderson-Darling statics based tests.

    Simulating first a Homogenous Poisson.
    """)
    from numpy.random import seed, exponential
    seed(20110928)
    hp1 = np.cumsum(exponential(1/242.5,2000))
    hp1 = hp1[hp1<6]
    hp1_d = discretize_time(hp1)
    hp1_dj = jitter_time(hp1_d)
    print("""
    The Kolmogorov and Anderson-Darling statistics are:
    """)
    D_W2_1 = {"D_o":Kolmogorov_D(hp1/6),
              "D_d":Kolmogorov_D(hp1_d/6),
              "D_dj":Kolmogorov_D(hp1_dj/6),
              "W2_o":AndersonDarling_W2(hp1/6),
              "W2_d":AndersonDarling_W2(hp1_d/6),
              "W2_dj":AndersonDarling_W2(hp1_dj/6)}
    res_out = "\n           original   discretized      jittered\n"
    res_out += "    D  {D_o:12.8f}  {D_d:12.8f}  {D_dj:12.8f}\n"
    res_out += "    W2 {W2_o:12.8f}  {W2_d:12.8f}  {W2_dj:12.8f}"
    print(res_out.format(**D_W2_1))
    print("""
    There is no 'huge' effect of time discretization here. The same is done after Durbin's transformation:
    """)
    hp1_dt = DurbinTransform(hp1,[0,6])
    hp1_d_dt = DurbinTransform(hp1_d,[0,6])
    if np.any(hp1_d_dt==0):
        hp1_d_dt[hp1_d_dt==0] = 5*np.finfo(float).eps 
        
    if np.any(hp1_d_dt==1):
        hp1_d_dt[hp1_d_dt==1] -= 5*np.finfo(float).eps 

    hp1_dj_dt = DurbinTransform(hp1_dj,[0,6])
    print("""
    The Kolmogorov and Anderson-Darling statistics are then:
    """)
    D_W2_2 = {"D_o":Kolmogorov_D(hp1_dt),
              "D_d":Kolmogorov_D(hp1_d_dt),
              "D_dj":Kolmogorov_D(hp1_dj_dt),
              "W2_o":AndersonDarling_W2(hp1_dt),
              "W2_d":AndersonDarling_W2(hp1_d_dt),
              "W2_dj":AndersonDarling_W2(hp1_dj_dt)}
    print(res_out.format(**D_W2_2))


    print("""
    Test against Loader and Deely reported results.

    Table IIaa
    """)

    ## test-against-tableIIaa
    LD87tableIIaa = [G_at_1_with_bounds(lambda x: np.sqrt(1+x),
                                        lambda x: 0.5/np.sqrt(1+x),n)
                     for n in [8,16,32,64,128]]

    [[str(round(x[0],5)),str(round(x[2],5))] for x in LD87tableIIaa]
    n_tableIIaa = [8,16,32,64,128]
    Gl_tableIIaa = [r[0] for r in LD87tableIIaa]
    Gu_tableIIaa = [r[2] for r in LD87tableIIaa]
    LD87tableIIaa_out = """Bounds for G(t) when the boundary is (1+t)^(1/2) using b(t)=0.5/(1+t)^(1/2):
    n  L & D Gl(1) Present Gl(1)  L & D Gu(1)  Present Gu(1)
    {n[0]:>3}      0.19524       {Gl[0]:.5f}      0.19690        {Gu[0]:.5f}
    {n[1]:>3}      0.19560       {Gl[1]:.5f}      0.19643        {Gu[1]:.5f}
    {n[2]:>3}      0.19580       {Gl[2]:.5f}      0.19621        {Gu[2]:.5f}
    {n[3]:>3}      0.19590       {Gl[3]:.5f}      0.19610        {Gu[3]:.5f}
    {n[4]:>3}      0.19595       {Gl[4]:.5f}      0.19605        {Gu[4]:.5f}
    """.format(n=n_tableIIaa,
               Gl=Gl_tableIIaa,
               Gu=Gu_tableIIaa)
    print(LD87tableIIaa_out)
    
