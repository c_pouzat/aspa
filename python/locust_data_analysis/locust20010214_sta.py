import numpy as np
import matplotlib.pylab as plt

import aspa

u4_C3H_1 = [float(line) for line in open("locust20010214_C3H_1_tetB_u4.txt")]
len(u4_C3H_1)

u4_C3H_1 = [x/15000 for x in u4_C3H_1]
u4_C3H_1_L = [[x-(x//30.0)*30.0 for x in u4_C3H_1
               if s_idx*30 <= x < (s_idx+1)*30]
              for s_idx in range(25)]
[len(s) for s in u4_C3H_1_L]

fig = plt.figure(figsize=(5,5))

aspa.raster_plot(u4_C3H_1_L)

plt.savefig("figs/st-u4-from-C3H_1-raster.png")
plt.close()
"figs/st-u4-from-C3H_1-raster.png"

fig = plt.figure(figsize=(5,5))

aspa.cp_plot(u4_C3H_1_L,what="wt",color='blue')
aspa.cp_plot(u4_C3H_1_L,what="norm",color='red')

plt.savefig("figs/st-u4-from-C3H_1-observed-cp.png")
plt.close()
"figs/st-u4-from-C3H_1-observed-cp.png"

u4_Spont_1 = [float(line)/15000 for line in open("locust20010214_Spontaneous_1_tetB_u4.txt")]
u4_Spont_1_nbtrials = len(set([x//30.0 for x in u4_Spont_1])) 
u4_Spont_1_nu = len(u4_Spont_1)/u4_Spont_1_nbtrials/29 
print("The discharge frequency of unit 4 during Spontaneous_1 was: ",u4_Spont_1_nu)

u4_Spont_2 = [float(line)/15000 for line in open("locust20010214_Spontaneous_2_tetB_u4.txt")]
u4_Spont_2_nbtrials = len(set([x//30.0 for x in u4_Spont_2])) 
u4_Spont_2_nu = len(u4_Spont_2)/u4_Spont_2_nbtrials/29 
print("The discharge frequency of unit 4 during Spontaneous_2 was: ",u4_Spont_2_nu)

u4_C3H_1_spsth = aspa.StabilizedPSTH(u4_C3H_1_L,spontaneous_rate=u4_Spont_1_nu,region=[-5,10],onset=10)
print(u4_C3H_1_spsth)

u4_C3H_1_sspsth = aspa.SmoothStabilizedPSTH(u4_C3H_1_spsth,[2.5,5,15,25,50])

fig = plt.figure(figsize=(5,5))

u4_C3H_1_sspsth.plot(color='blue',what="Cp vs bandwidth")
plt.title('Cp vs bandwidth')
plt.grid()

plt.savefig("figs/st-u4-from-C3H_1_sspsth-Cp-vs-bw.png")
plt.close()
"figs/st-u4-from-C3H_1_sspsth-Cp-vs-bw.png"

fig = plt.figure(figsize=(5,5))

u4_C3H_1_sspsth.plot(color='blue',alpha=0.01)
plt.title('Stabilized scale')
plt.grid()

plt.savefig("figs/st-u4-from-C3H_1_sspsth-99-band.png")
plt.close()
"figs/st-u4-from-C3H_1_sspsth-99-band.png"
